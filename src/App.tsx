import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { PrimerComponente } from "./components/PrimerComponente";
import { SegundoComponente } from "./components/SegundoComponente";
import { TercerComponente } from "./components/TercerComponente";

function App() {
  const year = new Date().getFullYear();

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <hr />
      <PrimerComponente year={year} />

      <hr />
      <SegundoComponente />

      <hr />

      <TercerComponente />
    </>
  );
}

export default App;
