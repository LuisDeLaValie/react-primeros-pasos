import React, { useEffect, useState } from "react";

// Usar el UseEffect para traer los datos cuadno se carge el componente
// Llamar a una Api para consultar los datos y mostrarlos
export const SegundoComponente = () => {
  const [usuarios, setUsuarios] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, seterror] = useState("");
  const getUsuario = () => {
    setTimeout(async () => {
      try {
        const response = await fetch("https://reqres.in/api/users?page=2");
        const { data } = await response.json();

        setUsuarios(data);
      } catch (error) {
        console.log(`error al traer los datos: ${error}`);
        seterror((error instanceof Error).message);
      }

      setLoading(false);
    }, 2000);
  };

  useEffect(() => {
    getUsuario();
  }, []);

  return loading ? (
    <p>... Cargando data</p>
  ) : error != "" ? (
    <p>{error}</p>
  ) : (
    <>
      <div>Dataos de la Api</div>

      <ul>
        {usuarios.map((usuario, i) => (
          <li key={i}>
            <img src={usuario.avatar} alt="avatar" width="20px" />
            {usuario.first_name} {usuario.last_name}
          </li>
        ))}
      </ul>
    </>
  );
};
