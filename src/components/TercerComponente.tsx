import React, { useState } from "react";

import "../styles/tercercomponente.css";

// Componente que maneja formulario y eventos

export const TercerComponente = () => {
  const [usuario, setUsuario] = useState({});

  const ontenerdatos = (e: React.FormEvent<HTMLElement>) => {
    e.preventDefault();

    const datos = e.target;
    const usuario = {
      nombre: datos.nombre.value,
      apellido: datos.apellido.value,
      sexo: datos.sexo.value,
      bio: datos.bio.value,
    };

    console.log(usuario);

    setUsuario(usuario);
  };

  const changeData = (
    e: React.ChangeEvent<
      HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
    >
  ) => {
    console.log(e.target.name);
    const dato = e.target;
    setUsuario({ ...usuario, [dato.name]: dato.value });
  };
  return (
    <>
      <h1>Componente Formulario</h1>

      {usuario ? (
        <p>
          Hola soy {usuario.nombre} {usuario.apellido} y soy {usuario.sexo}, te
          cuneto que yo {usuario.bio}
        </p>
      ) : null}

      <form onSubmit={ontenerdatos}>
        <h3>Identidad</h3>
        <input
          type="text"
          placeholder="Nombre"
          name="nombre"
          onChange={changeData}
        />
        <input
          type="text"
          placeholder="Apellido"
          name="apellido"
          onChange={changeData}
        />
        <select name="sexo" onChange={changeData}>
          <option value="hombre">Hombre</option>
          <option value="mujer">Mujer</option>
        </select>
        <textarea
          name="bio"
          placeholder="Biografia"
          onChange={changeData}
        ></textarea>

        <button type="submit">Enviar</button>
      </form>
    </>
  );
};
