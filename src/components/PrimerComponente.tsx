import React, { useState } from "react";

type Props = {
  year: number;
};


// Ejecision en donde usamos los props y el useState
export const PrimerComponente = (props: Props) => {
  const [year, setYear] = useState(props.year);

  const changeYear = (
    event: React.MouseEvent<HTMLButtonElement>,
    nnew: number
  ) => {
    event.preventDefault();

    setYear(year + nnew);
  };

  return (
    <>
      <h2>El año que deceas ver es:</h2>
      <strong>{year}</strong>
      <br />
      <button onClick={(e) => changeYear(e, +1)}>Incrementar</button>
      <button onClick={(e) => changeYear(e, -1)}>Decrementar</button>
    </>
  );
};
